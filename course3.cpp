#include <iostream>
#include <string>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "Camera.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>
#include <map>
#include <cstdlib>
// Dimension of the window
const GLuint WIDTH = 1200, HEIGHT = 1080;
GLfloat ANGLE_CREPUSC(3*M_PI/5);
GLfloat ANGLE_AUBE(4*M_PI/3);

    //cycle day/night
GLint DUREE_CYCLE(120);



// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
   
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // Initialization of GLFW
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwSwapInterval(1);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "MyWindow", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);
    // Variable global that allows to ask the GLEW library to find the modern
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }
    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);
    
    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/c3/default.vertexshader",
        "shaders/c3/default.fragmentshader");
    
    // Declare the positions and uv coordinates in the same buffer
    GLfloat vertices[] = {
        // Positions       // Colors         // UV
         300.5f, 0.0f,  300.5f, 1.0f, 0.0f, 0.0f, 10.0f, 10.0f, // Top right point
         300.5f, 0.0f, -300.5f, 0.0f, 1.0f, 0.0f, 10.0f, 0.0f, // Bottom right point
        -300.5f, 0.0f, -300.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // Bottom left point
        -300.5f, 0.0f,  300.5f, 1.0f, 1.0f, 0.0f, 0.0f, 10.0f, // Top left point
    };

    // The table of indices to rebuild our triangles
    GLushort indices[] = {
        0, 1, 3,   // First triangle
        1, 2, 3    // Second triangle
    };
    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO);
    // Bind the VAO
    glBindVertexArray(VAO);
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // UV coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    // Declare the texture identifier
    GLuint texture;
    // Generate the texture
    glGenTextures(1, &texture);
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture);
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char* data = SOIL_load_image("texture/grass.png",
        &twidth, &theight, 0, SOIL_LOAD_RGB);

    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight,
        0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // Camera
    Camera camera(glm::vec3(0.0f, 0.7f, 0.0f), window);
    
    // Creation of tall the different model  
    Model house("model/house/farmhouse_obj.obj");
    Model car("model/car/kamaz.3ds");
    Model lamp("model/lampe/streetlamp.obj");
    Model tree("model/tree/JoshuaTree.3ds");

    // Creation of the initial placement of the house/tree (islet)
    std::map<int, std::vector<GLfloat>> posIni;
    int k = 0;
    for(GLfloat i = 150;i >= -150.0f; i-=100.0f) {
        for(GLfloat j = 150;j >= -150.0f; j-=100.0f) {
            std::vector<GLfloat> vec;
            vec.push_back(i);
            vec.push_back(j);
            posIni[k] = vec;
            k++;
        } 
    }


    // Creation of a map which contains the random translation and rotation for each islet
    srand (static_cast <unsigned> (time(0)));
    std::map<int, std::vector<GLfloat>> dicoRand;
    for (int i = 0; i < posIni.size(); i++)
    {
        vector<GLfloat> v;
        GLfloat Tx = (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/30)));
        GLfloat Ty = (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/30)));
        GLfloat R = (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/3,14159)));
        v.push_back(Tx);
        v.push_back(Ty);
        v.push_back(R);
        for (int i = 0; i < 4; i++)
        {
            if(v[i] >= 7.5) {
                v[i] = -v[i];
            }
        }
        dicoRand[i] = v;
    }


    // Random vector to how many house will be drawn
    std::vector<int> drawOrNot;
    for (int i = 0; i < posIni.size(); i++) {
        int random = (static_cast <int> (rand()) / (static_cast <int> (RAND_MAX/2)));   
        drawOrNot.push_back(random); 
    }

    int numTree = 0;
    for (int i = 0; i < drawOrNot.size(); i++)
    {
        if (drawOrNot[i] == 1) {
            numTree += 1;
            std::cout<<numTree<<std::endl;
        }
        
    }
    

    // Creation of a map  which containe the random translation for the trees
    std::map<int, std::vector<int>> dicoTreeRand;
    for (int i = 0; i < 4; i++)
    {   
        vector<int> v;
        int Tx = (static_cast <int> (rand()) / (static_cast <int> (RAND_MAX/60)));
        int Ty = (static_cast <int> (rand()) / (static_cast <int> (RAND_MAX/60)));	
        v.push_back(Tx);
        v.push_back(Ty);
        for (int i = 0; i < 1; i++)
        {
            if(v[i] >= 5) {
                v[i] = -v[i];
            }
        }
        dicoTreeRand[i] = v;
    }


    // Initialisation of the speed and position of the car
    GLfloat vitesse = 2;
    glm::vec3 posCar = glm::vec3(250.0f,0.0f,250.0f);

    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        camera.Do_Movement();
        camera.Switch_Mode();

        // Replace the background color buffer of the window
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();

        // Recover the identifiers of the global variables of the shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");
        GLint ambientLightf = glGetUniformLocation(shaders.Program,"ambientStrength");
        GLint Specular = glGetUniformLocation(shaders.Program,"specularStrength");
        GLint lightPos = glGetUniformLocation(shaders.Program,"lightPos");
        GLint lampPos = glGetUniformLocation(shaders.Program,"lampPos");
        GLint lampColor = glGetUniformLocation(shaders.Program,"lampColor");
        GLint lightColor = glGetUniformLocation(shaders.Program,"lightColor");
        GLint viewPosLoc = glGetUniformLocation(shaders.Program,"viewPos");
        GLint testPos = glGetUniformLocation(shaders.Program,"testPos");
        GLint testColor = glGetUniformLocation(shaders.Program,"testColor");

      
     
       
        // Model matrix (translation, rotation and scale)
        glm::mat4 model = glm::mat4(1.0f);
	model = glm::scale(model, glm::vec3(0.1f,0.1f,0.1f));	
        model = glm::translate(model, glm::vec3(0.0f));
        //model = glm::scale(model, glm::vec3(0.1f,0.1f,0.1f));
        GLfloat angle = glm::mod(2*M_PI*(glfwGetTime()/DUREE_CYCLE), 2*M_PI);
        glm::vec4 lPos(10.0f, 10.0f, 0.0f, 1.0f);//camera.x...
        glm::vec3 lColor(1.0f, 1.0f, 1.0f);
        GLfloat ambStr(0.9f);
        GLfloat specStr(0.03f);
         glm::vec3 clearColor;
        //object.Draw(shaders); 
        if (angle > ANGLE_CREPUSC && angle < ANGLE_AUBE){
            lColor = glm::vec3(0.0f, 0.0f, 0.05f);

            //sky at night
            clearColor = glm::vec3(0.0f, 0.0f, 0.0f);

            //weak light ambiant
            ambStr = 0.2f;
            specStr = 0.5f;

        //night twilight
        } else if (angle > M_PI/4 && angle < ANGLE_CREPUSC){
            
            double x = (ANGLE_CREPUSC - angle)/(ANGLE_CREPUSC - M_PI/4);

            
            lColor = glm::vec3(x * 1.0f, pow(x, 2) * 1.0f, pow(x, 4) * 1.0f + 0.05f);

            
            clearColor = glm::vec3((-2*pow(x,2) + 2*x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);

            
            ambStr = x* 0.65f + 0.2f;
            


        //Lueur de l'aube
        } else if (angle > ANGLE_AUBE && angle < 5*M_PI/3){
            //use for the sky and th light
            double x = (ANGLE_AUBE - angle)/(ANGLE_AUBE - 5*M_PI/3);

            
            lColor = glm::vec3(pow(x,4) * 1.0f, pow(x, 2) * 1.0f, x * 1.0f + 0.05f);

            //increase sky at night
            clearColor = glm::vec3((-2*pow(x,2) + 2*x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);

            //light ambiant increase
            ambStr = x * 0.65f + 0.5f;
            

        //light day
        } else{
            
            lColor = glm::vec3(1.0f, 1.0f, 1.0f);

            //blue color of the sky
            clearColor = glm::vec3(0.0f, 0.5f, 1.0f);

            //light ambient at the day
            ambStr = 0.7f;
        }

        glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 100.0f);

        // Update the global variables of the shader
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        
            
            //At night no light
            glm::vec3 lColorl(0.0f,0.0f,0.0f);
            
            //Position of the light
            glUniform3f(lightPos,0.0f,50.0f,0.0f);
            glUniform3f(lightColor,lColor.x,lColor.y,lColor.z);
            
            
            glUniform3f(viewPosLoc, 10.0f, 10.0f, 0.0f);
            glUniform1f(ambientLightf,ambStr);
            glUniform1f(Specular,specStr);

        glm::vec3 lmpColor;
        if (angle > 2*M_PI/5 && angle < 8*M_PI/5){
            lmpColor = glm::vec3(1.0f, 1.0f, 1.0f);
        //At day, no light lamp
        } else {
            lmpColor = glm::vec3(0.0f, 0.0f, 0.0f);
        }
        
        //define the color of the light
        glUniform3f(lampColor, lmpColor.x, lmpColor.y , lmpColor.z);
        glUniform3f(testColor, lmpColor.x, lmpColor.y , lmpColor.z);




        //model = glm::translate(model, glm::vec3(20.0f,0.0f,0.0f));
        //lamp.Draw(shaders);
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));



        // Bind the VAO as a current object in the context of OpenGL
        glBindVertexArray(VAO);
        // Activate texture 0
        glActiveTexture(GL_TEXTURE0);
        // Bind the texture
        glBindTexture(GL_TEXTURE_2D, texture);
        // Associate the texture with the shader
        glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
        // Draw the current object
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
        // VBA is detached from the current object in the OpenGL context
        glBindVertexArray(0);

/*----------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------- DRAWING OBJECTS ------------------------------------------------------------------------
*/
        // Position of the house 
        for (int i = 0; i < posIni.size(); ++i)
        {       
            // Determine if we draw a house or a tree
            if(drawOrNot[i] == 0){ 
                model = glm::translate(model, glm::vec3(posIni[i][0] + dicoRand[i][0], 0.0f,posIni[i][1] + dicoRand[i][1]));
                model = glm::rotate(model, dicoRand[i][2],glm::vec3(0.0f,1.0f,0.0f));
                
                glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
                glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
                house.Draw(shaders);
        
                model = glm::rotate(model, dicoRand[i][2],glm::vec3(0.0f,-1.0f,0.0f));
                model = glm::translate(model, glm::vec3(-posIni[i][0] - dicoRand[i][0], 0.0f,-posIni[i][1] - dicoRand[i][1]));
            } else if (drawOrNot[i] == 1) {
                for (int j = 0; j < 4; ++j) {
                    model = glm::translate(model, glm::vec3(posIni[i][0] + dicoRand[i][0] + dicoTreeRand[j][0], 15.7f,posIni[i][1] + dicoRand[i][1] + dicoTreeRand[j][1]));
                    model = glm::rotate(model, (GLfloat)M_PI/2,glm::vec3(-1.0f,0.0f,0.0f));
                    model = glm::scale(model, glm::vec3(10.0f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
                    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
                    tree.Draw(shaders);
                    model = glm::scale(model, glm::vec3(0.1f,0.1f,0.1f));
                    model = glm::rotate(model, (GLfloat)M_PI/2,glm::vec3(1.0f,0.0f,0.0f));
                    model = glm::translate(model, glm::vec3(-posIni[i][0] - dicoRand[i][0] - dicoTreeRand[j][0], -15.7f,-posIni[i][1] - dicoRand[i][1] - dicoTreeRand[j][1]));
                
            }
        }
    }
    model = glm::translate(model, glm::vec3(290.0f, 0.0f,290.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lamp.Draw(shaders);
    model = glm::translate(model, glm::vec3(-290.0f, 0.0f,-290.0f));
    model = glm::translate(model, glm::vec3(-290.0f, 0.0f,290.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lamp.Draw(shaders);
    model = glm::translate(model, glm::vec3(290.0f, 0.0f,-290.0f));
    model = glm::translate(model, glm::vec3(290.0f, 0.0f,-290.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lamp.Draw(shaders);
    model = glm::translate(model, glm::vec3(-290.0f, 0.0f,290.0f));
    model = glm::translate(model, glm::vec3(-290.0f, 0.0f,-290.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lamp.Draw(shaders);
    model = glm::translate(model, glm::vec3(290.0f, 0.0f,290.0f));
    model = glm::translate(model, glm::vec3(220.0f, 0.0f,220.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lamp.Draw(shaders);
    model = glm::translate(model, glm::vec3(-220.0f, 0.0f,-220.0f));
    model = glm::translate(model, glm::vec3(-220.0f, 0.0f,220.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lamp.Draw(shaders);
    model = glm::translate(model, glm::vec3(220.0f, 0.0f,-220.0f));
    model = glm::translate(model, glm::vec3(220.0f, 0.0f,-220.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lamp.Draw(shaders);
    model = glm::translate(model, glm::vec3(-220.0f, 0.0f,220.0f));
    model = glm::translate(model, glm::vec3(-220.0f, 0.0f,-220.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
    lamp.Draw(shaders);
    model = glm::translate(model, glm::vec3(220.0f, 0.0f,220.0f));

        // Creation of each lamp, a bit messy but it's the only solution we find to manage the position light (complicated in a loop)
        model = glm::translate(model, glm::vec3(290.0f, 0.0f,290.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        lamp.Draw(shaders);
        model = glm::translate(model, glm::vec3(-290.0f, 0.0f,-290.0f));

        model = glm::translate(model, glm::vec3(-290.0f, 0.0f,290.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        lamp.Draw(shaders);
        model = glm::translate(model, glm::vec3(290.0f, 0.0f,-290.0f));

        model = glm::translate(model, glm::vec3(290.0f, 0.0f,-290.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        lamp.Draw(shaders);
        model = glm::translate(model, glm::vec3(-290.0f, 0.0f,290.0f));

        model = glm::translate(model, glm::vec3(-290.0f, 0.0f,-290.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        lamp.Draw(shaders);
        model = glm::translate(model, glm::vec3(290.0f, 0.0f,290.0f));


        
        model = glm::translate(model, glm::vec3(220.0f, 0.0f,220.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        lamp.Draw(shaders);
        model = glm::translate(model, glm::vec3(-220.0f, 0.0f,-220.0f));

        model = glm::translate(model, glm::vec3(-220.0f, 0.0f,220.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        lamp.Draw(shaders);
        model = glm::translate(model, glm::vec3(220.0f, 0.0f,-220.0f));

        model = glm::translate(model, glm::vec3(220.0f, 0.0f,-220.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        lamp.Draw(shaders);
        model = glm::translate(model, glm::vec3(-220.0f, 0.0f,220.0f));

        model = glm::translate(model, glm::vec3(-220.0f, 0.0f,-220.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        lamp.Draw(shaders);
        model = glm::translate(model, glm::vec3(220.0f, 0.0f,220.0f));

/*----------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------- DRAWING AND MOVING THE CAR ------------------------------------------------------------
*/
        // Move the car around the square
        if(posCar.z >= -250.0f && posCar.x >= 250.0f) {
            posCar.z -= vitesse;
            model = glm::translate(model, posCar);
        } else if(posCar.x >= -250.0f && posCar.z <= -250.0f) {
            posCar.x -= vitesse;
            model = glm::translate(model, posCar);
            model = glm::rotate(model, (GLfloat)M_PI/2,glm::vec3(0.0f,1.0f,0.0f));
        } else if(posCar.z <= 250.0f && posCar.x <= -250.0f) {
            posCar.z += vitesse;
            model = glm::translate(model, posCar);
            model = glm::rotate(model, (GLfloat)M_PI,glm::vec3(0.0f,1.0f,0.0f));
        } else if(posCar.x <= 250.0f && posCar.z >= 250.0f) {
            posCar.x += vitesse;
            model = glm::translate(model, posCar);
            model = glm::rotate(model, (GLfloat)M_PI/2,glm::vec3(0.0f,-1.0f,0.0f));
        }

        model = glm::scale(model, glm::vec3(5.0f,5.0f,5.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        car.Draw(shaders);
        
        model = glm::translate(model, glm::vec3(0.0f, 40.0f,0.0f));




        //Position of the light
        glm::vec3 lmpPos(0.0f, 100.f,5.0f);
        glm::vec3 tstPos(-50.0f, 100.0f,-10.0f);
    

    
    //One light at the "middle" of the plateform
    glUniform3f(lampPos, lmpPos.x, lmpPos.y , lmpPos.z);
    //One light at the top right
    glUniform3f(testPos, tstPos.x, tstPos.y , tstPos.z);
    
        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);
    }
   
    


    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}
