# OpenGL project in duo

Code by Romain Delannet and Louis Guilbaud
Master 2 TSI 2019

## Compile and run the project

1. Clone the git 
2. Change directory in the project
3. run cmake .
4. run ./course3

## Statement

Model an urban environment using OpenGL, by spatially placing objects (houses,
trees, lampposts) in a coherent way on a textured floor.

## What we implement 
- A dynamic light : cycle day/night, lamppost who turn on in the night
- A random generation of the world, random number and placement of house and tree
- A car that move aroud the square
- Two different options for the camera when push space button

## Difficulty encountered
- We did not succeed to easily create multiple light, so we had to multiply some line of code it give a code a little messy.
- We dont make a pure colision function when we made the random, instead we create some islet (16) and in each islet we made a random translation and rotation.


